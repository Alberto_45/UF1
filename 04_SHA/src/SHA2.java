import java.math.BigInteger;
import java.security.MessageDigest;

public class SHA2 {
    public static String getSHA256(String input){

        String toReturn = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            digest.reset();
            digest.update(input.getBytes("utf8"));
            toReturn = String.format("%064x", new BigInteger(1, digest.digest()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return toReturn;
    }

    public static String getSHA512(String input){

        String toReturn = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-512");
            digest.reset();
            digest.update(input.getBytes("utf8"));
            toReturn = String.format("%0128x", new BigInteger(1, digest.digest()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return toReturn;
    }

    public static void main(String[] argv) {

        String inputValue = "Alberto del Pozo";

        // Con librerias java sha256
        String sha256 = getSHA256(inputValue);

        System.out.println("El hash SHA-256 de '" + inputValue + "' es:");
        System.out.println(sha256);
        System.out.println();

        // Con librerias java sha512
        String sha512 = getSHA512( inputValue );

        System.out.println("El hash SHA-512 de '" + inputValue + "' es:");
        System.out.println(sha512);
        System.out.println();
        
    }
}
